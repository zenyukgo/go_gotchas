package main

import "testing"

func TestCopy_NeedToInitializeSliceBeforeUsingCopy(t *testing.T) {
	// arrange
	src := []string{"a", "b", "c"}
	dstNotInitialized := []string{}

	// act
	copy(dstNotInitialized, src)

	// assert
	if len(dstNotInitialized) != 3 {
		t.Error("Init destination slice with: dst := make([]string, len(src))")
	}
}

func TestCopy_SliceInitializedBeforeUsingCopy(t *testing.T) {
	// arrange
	src := []string{"a", "b", "c"}
	dstNotInitialized := make([]string, len(src))

	// act
	copy(dstNotInitialized, src)

	// assert
	if len(dstNotInitialized) != 3 {
		t.Error("Init destination slice with: dst := make([]string, len(src))")
	}
}
