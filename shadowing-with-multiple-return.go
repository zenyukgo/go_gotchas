package main

func main() {
	i := 1
	for {
		i, ok := getNext() // "i" is SHADOWING the other "i"
		println(i, ok)
		break
	
	}
	println(i)
}

func getNext() (int, bool) {
	return 2, true
}