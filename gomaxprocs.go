package main

import "fmt"
import "time"
import "runtime"

// run with:
// $ GOMAXPROCS=8 go run gomaxprocs.go

func main() {
    var result int
	processors := runtime.GOMAXPROCS(0) 
    for i := 0; i < processors; i++ { // change to " < processors - 1" for program to stop
        go func() {
            for { result++ }
        }()
    }
    time.Sleep(time.Second)       //wait for go function to increment the value.
    fmt.Println("result =", result)
}