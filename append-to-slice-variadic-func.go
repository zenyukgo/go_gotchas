package main

import (
	"fmt"
)

func change(s []string) {
    s[0] = "will_be_updated"
    s = append(s, "will_be_missing_in_main")
    fmt.Println(s)
}

func main() {
    welcome := []string{"hello", "world"}
    change(welcome)
    fmt.Println(welcome)
}