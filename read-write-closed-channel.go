package main

import "fmt"

func main() {
	readFromClosedChannel()
	readTwiceFromClosedChannel()
	writeToClosedChannel()
}

func readFromClosedChannel() {
	c := make(chan int)

	go func() {
		c <- 1
		close(c)
	}()

	v, ok := <-c
	if ok {
		print("readFromClosedChannel ok - ")
	}
	println(v)
}

func readTwiceFromClosedChannel() {
	c := make(chan int)

	go func() {
		c <- 1
		close(c)
	}()

	v1, ok := <-c
	if ok {
		print("readTwiceFromClosedChannel, first try ok")
	}
	fmt.Printf("First read value is %d\n", v1)

	v2, ok := <-c
	if ok {
		print("readTwiceFromClosedChannel, second try ok - ")
	}
	fmt.Printf("Second read value is %d\n", v2)
}

func writeToClosedChannel() {
	defer func() {
		err := recover()
		if err != nil {
			fmt.Printf("recoverd from error: %s", err)
		}
	}()

	c := make(chan int)

	go func() {
		c <- 1
		close(c)
	}()

	v1, ok := <-c
	if ok {
		print("writeToClosedChannel, first read is ok - ")
	}
	println(v1)

	c <- 2 // this will panic

	// all below will not execute
	v2, ok := <-c
	if ok {
		print("writeToClosedChannel, second read is ok - ")
	}
	println(v2)
}
